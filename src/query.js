import { SelectorParser } from './selector';


/*
Type: SELECT

   {
      type: "select"
      columns: { ... }
      table: { name, as } | ...
      joins: [
         { type: "join", ... }
         ...
      ]
      where: { ... }
      groupBy: ...
      orderBy: ...
      offset: ...
      limit: ...
   }
*/
const typeSelect = (query, ctx) => {
   let {
      table,
      joins,
      columns,
      where,
      groupBy,   // TODO
      orderBy,   // TODO
      offset,
      limit
   } = query;

   // 1. table

   const tables = [ctx.pushTable(table)];
   
   where = { [ctx.currentTable]: where };
   
   // 2. columns

   ctx.setColumns(columns);

   // 3. joins

   if (joins) {
      if (!Array.isArray(joins)) {
         throw Error('Joins must be an array');
      }

      tables.push(...joins.map(join => ctx.builder.types.join(join, ctx)));
   }

   // 4. where

   const conditionsDef = ctx.parseSelector(where);

   // 5. Group by


   // 6. Order by


   // 7. limit / offset

   if (limit > 0) {
      const limitPlaceholder = ctx.builder.selectorParser.formatValue(limit, ctx.params);

      limit = `LIMIT ${limitPlaceholder}`;
      
      if (offset > 0) {
         const offsetPlaceholder = ctx.builder.selectorParser.formatValue(offset, ctx.params);

         limit = `${limit} OFFSET ${offsetPlaceholder}`;
      }
   } else {
      limit = null;   // no limit
   }

   // ... build

   const tablesDef = tables.join(' ');
   const columnsDef = ctx.builder._buildColumns(ctx.columns, ctx);
   const filterDef = [ limit ].filter(x => x).join(' ');

   // ... cleanup

   ctx.popTable();

   const sql = (`SELECT ${columnsDef} FROM ${tablesDef} WHERE ${conditionsDef} ${filterDef}`).trim();

   if (ctx.wrapQuery) {
      return `(${sql})`;
   } else {
      return sql;
   }
};


/*
Type: INSERT

   {
      type: "insert"
      columns: { ... }
      table: { name, as } | ...
      values: { ... } | [
         { ... }
      ]
      select: { type: "select", ... }
      returning: true | false
   }
*/
const typeInsert = (query, ctx) => {
   let {
      columns,
      table,
      values,
      select,       // TODO
      returning,    // TODO
   } = query;

   // 1. table

   ctx.pushTable(table);

   const tableDef = ctx.tableStack[ctx.tableStack.length - 1];
   
   // 2. columns

   const columnsDef = Object.keys(columns).map(alias => columns[alias] === true ? alias : columns[alias]).join(',');

   // 3. values

   if (!Array.isArray(values)) {
      values = [values];
   }

   const valuesDef = values.map(doc => '(' + Object.keys(columns).map(key => doc[key] === DEFAULT_INSERT_VALUE ? 'DEFAULT' : ctx.addParam(doc[key])).join(',') + ')').join(',');

   // ... cleanup
   
   ctx.popTable();

   const sql = `INSERT INTO ${tableDef} (${columnsDef}) VALUES ${valuesDef}`;

   return sql;
};


/*
Type: UPDATE

   {
      type: "update"
      columns: { ... }
      table: { name, as } | ...
      values: { ... }
      where: { ... }
      limit: ...
   }
*/
const typeUpdate = (query, ctx) => {
   let {
      table,
      columns,
      values,
      where,
      limit,
   } = query;

   // 1. table

   ctx.pushTable(table);

   const tableDef = ctx.tableStack[ctx.tableStack.length - 1];

   // 3. values

   const valuesDef = Object.keys(columns).filter(alias => columns[alias]).map(alias => {
      if (alias in values) {
         const col = columns[alias] === true ? alias : columns[alias];
         const value = ctx.addParam(values[alias]);

         return `${col} = ${value}`;
      } else {
         return '';
      }
   }).filter(x => x).join(', ');

   if (!valuesDef) {
      throw Error('Nothing to update!');
   }
         
   // 4. selector

   const conditionsDef = ctx.parseSelector(where);

   // 5. filter

   if (limit > 0) {
      const limitPlaceholder = ctx.builder.selectorParser.formatValue(limit, ctx.params);

      limit = `LIMIT ${limitPlaceholder}`;
   } else {
      limit = null;   // no limit
   }

   // ... build

   const filterDef = [ limit ].filter(x => x).join(' ');

   // ... cleanup
   
   ctx.popTable();

   const sql = (`UPDATE ${tableDef} SET ${valuesDef} WHERE ${conditionsDef} ${filterDef}`).trim();

   return sql;
};


/*
Type: DELETE

   {
      type: "delete"
      table: { name, as } | ...
      where: { ... }
      limit: ...
   }
*/
const typeDelete = (query, ctx) => {
   let {
      table,
      where,
      limit,
   } = query;

   // 1. table

   ctx.pushTable(table);

   const tableDef = ctx.tableStack[ctx.tableStack.length - 1];

   // 2. selector

   const conditionsDef = ctx.parseSelector(where);

   // 5. filter

   if (limit > 0) {
      const limitPlaceholder = ctx.builder.selectorParser.formatValue(limit, ctx.params);

      limit = `LIMIT ${limitPlaceholder}`;
   } else {
      limit = null;   // no limit
   }

   // ... build

   const filterDef = [ limit ].filter(x => x).join(' ');

   // ... cleanup
   
   ctx.popTable();

   const sql = (`DELETE FROM ${tableDef} WHERE ${conditionsDef} ${filterDef}`).trim();

   return sql;
};



/*
Type: JOIN

   { 
      type: "join"
      join: 'inner" | "left" | "right" | ...
      columns: { ... }
      table: { name, as } | ...
      on: { ... }
   }

*/
const typeJoin = (query, ctx) => {
   let {
      join,
      columns,
      table,
      on
   } = query;

   // 1. join type

   if (!join) {
      join = '';
   } else if (typeof join !== 'string') {
      throw Error('Invalid join type value');
   } else {
      join = join.toLocaleUpperCase();
   }

   // 2. table

   const joinTable = ctx.pushTable(table);

   on = { [ctx.currentTable]: on };

   // 3. columns

   ctx.setColumns(columns);

   // 4. on condition

   const conditionsDef = ctx.parseSelector(on);

   // ... cleanup

   ctx.popTable();
   
   return (`${join} JOIN ${joinTable} ON ${conditionsDef}`).trim();
};


/*
Type: EXPRESSION

   {
      type: "expression"
      value: ...
   }
*/
const typeExpression = (query, ctx) => {
   const { content } = query;

   if (typeof content === 'string') {
      return content;
   } else if (Array.isArray(content)) {
      return content.map(part => {
         if (typeof part === 'string') {
            return part;
          } else {
            const type = part && part.type;
            const helper = ctx.builder.types[type];

            if (helper) {
               return helper(part, ctx);
            } else {
               return '';
            }
         }
      }).join('');
   } else {
      return 'NULL';
   }
};


/*
Type: FIELD

   {
      type: "field"
      name: ...
   }
*/
const typeField = (query, ctx) => {
   const { name } = query;
   const table = ctx.getTable();

   return ctx.builder.selectorParser.formatColumnNames([ table, name ]);
}


const typeParam = (query, ctx) => {
   const { value } = query;

   return ctx.addParam(value);
}




const standardTypes = {
   select: typeSelect,
   insert: typeInsert,
   update: typeUpdate,
   delete: typeDelete,

   join: typeJoin,

   expression: typeExpression,
   field: typeField,
   param: typeParam,
};


const DEFAULT_INSERT_VALUE = Symbol('DEFAULT');

const $tablePrefix = Symbol('$tablePrefix');
const $tableCount = Symbol('$tableCount');
const $builderFieldHelper = Symbol('$builderFieldHelper');

class QueryBuilderContext {

   // private

   [$tablePrefix] = 'tbl';
   [$tableCount] = 0;

   /**
    * Block types should be wrapped in parenthesess
    */
   wrapQuery = false;

   // public

   /**
    * The current table alias (i.e. not name)
    */
   currentTable = null;
   /**
    * Map table name -> table alias
    */
   tableMap = {};
   /**
    * A list of table names (i.e. not alias)
    */
   tableStack = [];

   /**
    * All the columns to return
    */
   columns = {};

   /**
    * All the params to return
    */
   params = [];


   constructor(builder) {
      this.builder = builder;

      // backup helper...
      this[$builderFieldHelper] = builder.selectorParser.helpers.$field;

      // prepare selector parser to accept a special helper
      builder.selectorParser.helpers.$field = (path, value, ctx) => {
         let table = ctx.$table || this.getTable();

         if (table) {
            if (this.tableMap[table]) {
               table = this.tableMap[table];
            }

            value = [ table, value ];
         } else {
            value = [ value ];
         }

         if (path.length) {
            return `${builder.selectorParser.formatColumnNames(path)} = ${builder.selectorParser.formatColumnNames(value)}`;
         } else {
            return builder.selectorParser.formatColumnNames(value);
         }
      };
   }


   clone() {
      const clone = new QueryBuilderContext(this.builder);
      clone[$tableCount] = this[$tableCount];
      clone.currentTable = this.currentTable;
      clone.tableMap = this.tableMap;
      clone.tableStack = this.tableStack;
      clone.params = this.params;

      return clone;
   }

   dispose() {
      // restore helper if one was set
      if (this[$builderFieldHelper]) {
         this.builder.selectorParser.helpers.$field = this[$builderFieldHelper];
      } else {
         delete this.builder.selectorParser.helpers.$field;
      }
   }


   /**
    * Set the current table, and updating tableMap.
    * @param {String|Object} table 
    * @return {String}       the full table name with alias (if provided) to use
    */
   pushTable(table) {
      if (typeof table === 'string') {
         table = { name: table, as: this[$tablePrefix] + (++this[$tableCount]) };
      } else if (!table.name) {
         throw Error('No table name specified in block type');
      }
   
      this.tableStack.push(table.name);
   
      if (table.as) {
         this.currentTable = table.as;
         this.tableMap[table.name] = table.as;
   
         return `${table.name} ${table.as}`;
      } else {
         this.currentTable = table.name;
         this.tableMap[table.name] = table.name;
   
         return table.name;
      }
   }

   /**
    * Go back one table, setting the current table to the previous one
    */
   popTable() {
      this.tableStack.pop();
      if (this.tableStack.length) {
         this.currentTable = this.tableMap[this.tableStack[this.tableStack.length - 1]];
      } else {
         this.currentTable = null;
      }
   }

   /**
    * Return the table at the given depth, where 0 returns `this.currentTable`
    * @param {Number} depth 
    * @return {String}
    */
   getTable(depth) {
      if (!depth) {
         return this.currentTable;
      } else if (depth < this.tableStack.length) {
         const tableName = this.tableStack[this.tableStack.length - depth - 1];

         return this.tableMap[tableName];
      } else {
         return null;
      }
   }


   /**
    * Merge more columns using the current table name.
    * This method will check for collision!
    * @param {Object} columns 
    */
   setColumns(columns) {
      if (columns) {
  
         for (const col in columns) {
            let colValue = columns[col];

            if (typeof colValue === 'string') {
               if (this.columns[col]) {
                  throw Error('Column name collision detected : ' + col);
               }

               colValue = this.builder.selectorParser.formatColumnNames([this.currentTable, colValue]);
            }
            if (colValue) {
               this.columns[col] = colValue;
            }
         }
      }
   }

   /**
    * Parse the given selector and update params
    * @param {Object} selector 
    */
   parseSelector(selector) {
      return this.builder.selectorParser.parse(selector, this.params);
   }

   /**
    * Add a parameter and return it's placeholder
    * @param {any} value
    * @return {String}
    */
   addParam(value) {
      if (value && value.type && this.builder.types[value.type]) {
         const otherCtx = this.clone();
         otherCtx.wrapQuery = true;

         return this.builder.types[value.type](value, otherCtx);
      } else {
         this.params.push(value);

         return this.builder.selectorParser.getPlaceholder(this.params.length);
      }
   }

}






class QueryBuilder {

   types = standardTypes;


   constructor(options) {
      const {
         selectorParser
      } = options || {};

      if (!(selectorParser instanceof SelectorParser)) {
         throw Error('Invalid SelectorParser');
      }

      this.selectorParser = selectorParser;
   }


   /**
   Columns

      {
         "alias": "column_name" | { type: "select", ... } | { type: "expression", ... }
         ...
      }

   @param {Object} columns
   @param {Object} ctx       optional context, see the build method
   @return {String}
   */
   _buildColumns(columns, ctx) {
      const cols = [];

      columns = columns || {};

      for (let alias in columns) {
         let col = columns[alias];

         if (col.type in this.types) {
            const otherCtx = ctx.clone();
            otherCtx.wrapQuery = true;

            col = this.types[col.type](col, otherCtx);
         } else if (col === true) {
            col = this.selectorParser.formatColumnNames([ctx.currentTable, alias]);
         } else if (typeof col !== 'string') {
            throw Error('Invalid column, expected string for : ' + alias);
         }

         cols.push(`${col} AS \`${alias}\``);
      }

      if (!cols.length) {
         if (ctx.currentTable) {
            cols.push(`${ctx.currentTable}.*`);
         } else {
            cols.push('*');
         }
      }

      return cols.join(', ');
   }


   /**
   Usage:
   
      const [ sql, params ] = builder.build({ type: 'select', ... });

   @param {Object} query
   @return {Array<String,Array<mixed>>}
    */
   build(query) {
      if (!query || !this.types[query.type]) {
         throw Error('INvalid query');
      }

      const context = new QueryBuilderContext(this);

      const sql = this.types[query.type](query, context);

      // ... cleanup

      context.dispose();

      return [ sql, context.params ];
   }

};


export { QueryBuilder, standardTypes, DEFAULT_INSERT_VALUE };