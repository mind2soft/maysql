import { DatabaseProvider, DatabaseConnection } from './provider';
import { QueryBuilder } from './query';
import { SelectorParser } from './selector';


// Result SELECT
// [
//   [
//     TextRow: {
//       name: 'foo'
//     },
//     ...
//   ],
//   [
//     ColumnDefinition {
//       ...
//       characterSet: 224,
//       encoding: 'utf8',
//       name: 'name',
//       columnLength: 128,
//       columnType: 253,
//       flags: 4097,
//       decimals: 0
//     }
//   ]
//  ]


// Result INSERT (one)
// [
//    ResultSetHeader {
//      fieldCount: 0,
//      affectedRows: 1,
//      insertId: 18,
//      info: '',
//      serverStatus: 2,
//      warningStatus: 0
//    },
//    undefined
//  ]
// 
// NOTE : insertId = first id inserted => no gua


// Result:UPDATE
// [
//    ResultSetHeader {
//      fieldCount: 0,
//      affectedRows: 1,
//      insertId: 0,
//      info: 'Rows matched: 1  Changed: 1  Warnings: 0',
//      serverStatus: 2,
//      warningStatus: 0,
//      changedRows: 1
//    },
//    undefined
//  ]


// Result: DELETE
// [
//    ResultSetHeader {
//      fieldCount: 0,
//      affectedRows: 5,
//      insertId: 0,
//      info: '',
//      serverStatus: 2,
//      warningStatus: 0
//    },
//    undefined
//  ]



class MySqlDatabaseConnection extends DatabaseConnection {
   constructor(conn) {
      super();

      this.conn = conn;
   }

   async query(sql, values) {
      const [ result ] = await this.conn.query(sql, values);

      if (Array.isArray(result)) {
         return {
            rows: result,
            affectedRows: result.length,
         };
      } else {
         return {
            affectedRows: result.affectedRows,
            changedRows: result.changedRows,
            insertId: result.insertId
         };        
      }
      
   }
}



class MySqlQueryBuilder extends QueryBuilder {

   constructor(collection, options) {
      super(collection, {
         ...options,
         selectorParser: new SelectorParser()
      });
   }

}



class MySqlProvider extends DatabaseProvider {

   constructor(mysql, config) {
      super();

      this.pool = mysql.createPool( config );
   }


   async getConnection(handler) {
      const pool = this.pool;

      return new Promise((resolve, reject) => {
         pool.getConnection((err, conn) => {
            if (err) {
               reject(error);
            } else {
               const connection = new MySqlDatabaseConnection(conn);

               Promise.resolve().then(() => handler(connection)).then(resolve, reject).then(() => {
                  pool.releaseConnection(conn);
               });
            }
         });
      });
   }

   getQueryBuilder(collection) {
      return new MySqlQueryBuilder(collection, {
         provider: this
      });
   }

}



export default MySqlProvider;