

const isNull = v => (v === undefined) || (v === null) || (Array.isArray(v) && !v.length);

const isPrimitive = v => (v === null) || (typeof v !== 'object') || !isNaN(v) || (v instanceof Date) || (v instanceof RegExp)

const isBoolean = v => v === true || v === false;

const isRegExp = v => v instanceof RegExp;

const checkArray = v => {
   if (Array.isArray(v)) {
      return v;
   } else {
      throw new Error('Value must be an array');
   }
};

const defaultExpr = (path, op, value, parser, params) => `${parser.formatColumnNames(path)}${op}${parser.formatValue(value, params)}`;

/**
 * Standard selectors : add new implements or override whenever ncessary
 * 
 * TODO : redesign this to support helper priority
 */
const standardHelpers = {
   $eq: (path, value, ctx, parser, params) => isRegExp(value) ? standardHelpers.$regex(path, value, ctx, parser, params) : defaultExpr(path, isNull(value) ? ' IS ' : ' = ', value, parser, params),
   $ne: (path, value, ctx, parser, params) => isRegExp(value) ? standardHelpers.$regex(path, value, { ...ctx, $negate: true }, parser, params) : defaultExpr(path, isNull(value) ? ' IS NOT ' : ' <> ', value, parser, params),
   $gt: (path, value, _, parser, params) => defaultExpr(path, ' > ', value, parser, params),
   $gte: (path, value, _, parser, params) => defaultExpr(path, ' >= ', value, parser, params),
   $lt: (path, value, _, parser, params) => defaultExpr(path, ' < ', value, parser, params),
   $lte: (path, value, _, parser, params) => defaultExpr(path, ' <= ', value, parser, params),

   $in: (path, value, _, parser, params) => defaultExpr(path, ' IN ', checkArray(value), parser, params),
   $nin: (path, value, _, parser, params) => defaultExpr(path, ' NOT IN ', checkArray(value), parser, params),

   $regex: (path, value, ctx, parser, params) => {
      // NOTE : the $negate operator is not part of MongoDB's specifications, but it is a shortcut here. The official way to negate a regexp is to { $not: { $regxp: ... } }
      const ignoreCase = ((value instanceof RegExp) && value.ignoreCase) || (ctx && ctx.$options && (String(ctx.$options).toLowerCase().indexOf('i') >= 0));
      const negate = ctx && ctx.$negate;
      const op = negate ? ' NOT REGEXP ' : ' REGEXP ';

      if ((value instanceof RegExp) && ignoreCase) {
         let flags = value.flags;

         if (flags.indexOf('i') === -1) {
            flags = flags + 'i';
         }

         value = new RegExp(value.source.toLowerCase(), flags);
      }

      return ignoreCase ? 'LOWER(' + defaultExpr(path, ')' + op, value, parser, params) : defaultExpr(path, op, value, parser, params);
   },

   $and: (path, value, _, parser, params) => {
      const parsed = parseSelector(path, value, parser, ' AND ', params);
      return parsed ? '(' + parsed + ')' : '';
   },
   $or: (path, value, _, parser, params) => {
      const parsed = parseSelector(path, value, parser, ' OR ', params);
      return parsed ? '(' + parsed + ')' : '';
   },

   $not: (path, value, _, parser, params) => 'NOT (' + parseSelector(path, [value], parser, ' AND ', params) + ')',
   $nor: (path, value, _, parser, params) => 'NOT (' + parseSelector(path, [value], parser, ' OR ', params) + ')',
};



const parseSelector = (path, selector, parser, joinOper, params) => {
   if (!path.length && (!selector || isPrimitive(selector))) {
      return '';
   }

   const parts = [];
   const isArraySelector = Array.isArray(selector);
   const helpers = [];

   for (let key in selector) {
      const isFn = key && String(key).startsWith('$');
      const helper = isFn && parser.helpers && parser.helpers[key] ? parser.helpers[key] : null;
   
      if (helper) {
         helpers.push([ key, helper ]);
      }
   }

   if (helpers.length) {
      let value = undefined;

      for (const [ key, helper ] of helpers) {
         if (value === undefined) {
            value = selector[key];
         }

         value = helper(path, value, selector, parser, params);
      }

      parts.push( value );
   } else {
      for (const key in selector) {
         const value = selector[key];
         const isMeta = key.startsWith('$');
         
         if (!isMeta && (!isArraySelector || path.length)) {
            const primitive = isPrimitive(value);
            const newPath = isArraySelector ? path : path.concat([key]);

            if (primitive) {
               if (parser.helpers.$eq) {
                  parts.push( parser.helpers.$eq(newPath, value, selector, parser, params) );
               }
            } else {
               parts.push( parseSelector(newPath, value, parser, joinOper, params));
            }
         }
      }
   }

   return parts.filter(x => x).join(joinOper);
};




class SelectorParser {

   /**
   If true, will put placholders instead of values
   */
   useParams = true;

   helpers = standardHelpers;


   /**
   Return the placeholder token for the given param index
   @param {Number} index
   @return {String}
   */
   getPlaceholder(index) {
      return `$${index}`;
   }


   /**
   Parse the given selector and return a string

   @param {Object} selector
   @param {Array} params      ignored if useParams = false
   @return {String}
   */
   parse(selector, params) {
      params = params || [];      
      return parseSelector([], selector, this, ' AND ', params);
   }

   /**
   Format the column names so they are properly escaped

   @param {Array} path
   @return {String}
   */
   formatColumnNames(path) {
      return path.join('.');
   }

   /**
   Format the given argument and return a primitive value that can be used in a SQL query

   @param {mixed} value
   @param {Array} params
   @param {mixed}
   */
   formatValue(value, params) {
      if (Array.isArray(value)) {
         return '(' + value.map(val => this.formatValue(val, params)).join(',') + ')';
      } else {
         if (isNull(value)) {
            if (this.useParams) {
               value = null;
            } else {
               value = 'NULL';
            }
         } else if (isBoolean(value)) {
            if (this.useParams) {
               value = !!value;
            } else {
               value = value.toString().toUpperCase();
            }
         } else if (isRegExp(value)) {
            if (this.useParams) {
               value = value.source;
            } else {
               value = JSON.stringify(value.source);
            }
         } else {
            if (!this.useParams) {
               value = JSON.stringify(value);
            }
         }

         if (this.useParams) {
            const index = params.length + 1;

            params.push(value);

            return this.getPlaceholder(index);
         } else {
            return value;
         }
      }
   }

}



export { SelectorParser, standardHelpers, isNull, isPrimitive };