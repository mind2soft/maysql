import { DatabaseProvider } from './provider';
import { DEFAULT_INSERT_VALUE } from './query';


const column_types = {
   [Boolean]: true,
   [Number]: true,
   [String]: true,
   [Date]: true,
}


class Collection {


   /**
   Create a new collection with the specified table name
   */
   constructor(name, columns, options) {
      if (!name || (typeof name !== 'string')) {
         throw Error('Collection must have a valid name');
      } else if (!columns || !Object.keys(columns).length) {
         throw Error('No columns defined for collection');
      }

      const {
         table = name,   // table name defaults to collection name
         provider,
         primaryKey,
         relations,
      } = options || {};

      if (!table || (typeof table !== 'string')) {
         throw Error('Invalid table name');
      } else if (!(provider instanceof DatabaseProvider)) {
         throw Error('Invalid provider');
      }

      this.provider = provider;
      this.queryBuilder = provider.getQueryBuilder();
      this.relations = relations;

      this.name = name,
      this.table = table;
      this.primaryKey = primaryKey;
      this.columnTypes = {};
      this.columns = {};

      for (let col in columns) {
         const colType = columns[col];

         if (!(colType in column_types)) {
            throw Error(`Invalid column type for ${col} : ${colType}`);
         }

         if (col.startsWith('@')) {
            col = col.substr(1);

            if (!this.primaryKey || this.primaryKey === col) {
               this.primaryKey = col;
            } else {
               throw Error('A collection may have only one primary key');
            }
         }
            
         this.columns[col] = true;
         this.columnTypes[col] = colType;
      }
      
      if (!this.primaryKey) {
         throw Error('No primary key specified');
      } else if (typeof this.primaryKey !== 'string') {
         throw Error('Primary key must be a string');
      } else if (!(this.primaryKey in this.columns)) {
         throw Error('Primary key is not in columns');
      } else if (!validRelations(this.relations)) {
         throw Error('Invalid relation definitions');
      }
   }


   async _find(connection, selector, options) {
      const {
         fields,
         limit,
      } = options || {};

      const relationCache = {};
      const ignoreKeyPaths = {};
      const columns = { ...this.columns };
      const joins = [];

      // TODO: if 'fields' is provided, make sur ethat all primary keys are not ignored,
      //       only when constructing the documents

      if (fields) {
         for (const field in fields) {
            if (!field) {
               ignoreKeyPaths[field] = true;
               continue;
            }

            const includeField = !!fields[field];
            const fieldParts = field.split('.');
            let prevCollection = this;
            let collection = null;
   
            for (let i = 0, len = fieldParts.length; i < len; ++i) {
               const fieldPart = fieldParts[i];
               const subFieldParts = fieldParts.slice(0, i + 1);
               const fieldPath = subFieldParts.join('.');
               const parentIncluded = subFieldParts.every((_, index) => !(subFieldParts.slice(0, index).join('.') in ignoreKeyPaths));

               if (parentIncluded && prevCollection.relations && (fieldPart in prevCollection.relations)) {
                  const relation = prevCollection.relations[fieldPart];
                  
                  // next collection...
                  collection = this.provider.collections[relation.collection];

                  if (collection) {
                     if (!(fieldPath in relationCache)) {
                        const sourceKey = relation.key || prevCollection.primaryKey;
                        const foreignKey = relation.foreignKey || collection.primaryKey;
                        const joinColumns = Object.keys(collection.columns).reduce((columns, column) => {
                           if (column === foreignKey && !includeField) {
                              ignoreKeyPaths[`${fieldPath}.${column}`] = true;
                           }
                           if (column === foreignKey || includeField) {
                              columns[`${fieldPath}.${column}`] = column;
                           }

                           return columns;
                        }, {});

                        joins.push({
                           type: 'join',
                           columns: joinColumns,
                           table: collection.table,
                           on: { [foreignKey]: { $field: sourceKey, $table: prevCollection.table } }
                        });

                        // save path to primary key for row...
                        relationCache[fieldPath] = { 
                           collection,
                           docKey: fieldPart,
                           hasMany: relation.hasMany,
                           primaryKeyPath: fieldPath + '.' + collection.primaryKey
                        };
                     }
                     
                     prevCollection = collection;
                  } else {
                     throw Error(`Missing collection ${relation.collection} from relation ${fieldPath}.* in collection ${this.name}`);
                  }
               } else if (fieldPart in prevCollection.columns) {
                  if (!includeField) {
                     if (fieldPart === prevCollection.primaryKey) {
                        ignoreKeyPaths[fieldPath] = true;
                     } else if (i === 0) {
                        delete columns[fieldPart];
                     } else {
                        joins.forEach(join => {
                           if ((join.table === collection.table) && (fieldPart in join.columns)) {
                              delete join.columns[fieldPath];
                           }
                        });
                     }
                  }
               } else {
                  throw Error(`Unknown field in collection ${this.name} : ${field}`);
               }
            }
         }
      }

      const [ sql, values ] = this.queryBuilder.build({
         type: 'select',
         columns: columns,
         table: this.table,
         where: selector,
         joins,
         limit,
      });

      const { rows = [] } = await connection.query(sql, values);
      const documents = [];
      const docMap = {};
      
      for (const row of rows) {
         
         for (const col in row) {
            if (ignoreKeyPaths[col]) continue;
            
            const path = col.split('.');
            let docKey = row[this.primaryKey];
            let doc = docMap[docKey];
            let hasValue = true;

            if (!doc) {
               doc = docMap[docKey] = {};
               documents.push(doc);
            }
   
            for (let i = 0, len = path.length - 1; i < len && hasValue; ++i) {
               const curPath = path.slice(0, i + 1).join('.');
               const relation = relationCache[curPath];
               const curDocKey = relation ? row[relation.primaryKeyPath] : null;

               // do we have data for this row?
               if (curDocKey) {
                  let nextDocKey = curPath + '$' + row[relation.primaryKeyPath];
                  let nextDoc = docMap[nextDocKey];
                  
                  if (!nextDoc) {
                     nextDoc = docMap[nextDocKey] = {};

                     if (relation.hasMany) {
                        if (!doc[relation.docKey]) {
                           doc[relation.docKey] = [];
                        }

                        doc[relation.docKey].push(nextDoc);
                     } else {
                        doc[relation.docKey] = nextDoc;
                     }
                  }

                  doc = nextDoc;
               } else {
                  hasValue = false;
               }
            }

            if (hasValue) {
               doc[path[path.length - 1]] = row[col];
            }
         }
      }

      return documents;
   }

   async _insert(connection, documents, returning) {
      const key = this.primaryKey;
      const newKeys = [];
      const includeKeys = {};

      // TODO : begin transaction

      for (const document of documents) {
         const [ sql, values ] = this.queryBuilder.build({
            type: 'insert',
            columns: this.columns,
            table: this.table,
            values: [ { ...document, [key]: DEFAULT_INSERT_VALUE } ],
         });

         const { insertId } = await connection.query( sql, values );
         newKeys.push(insertId);

         await this._insertOrUpdateRelations(connection, this, [], document, insertId, includeKeys);
      }

      // TODO : end transaction

      if (returning) {
         return this._find(connection, { [key]: { $in: newKeys } }, { fields: includeKeys });
      } else {
         return newKeys;
      }
   }


   async _update(connection, selector, partial, options) {
      const {
         limit
      } = options || {};

      const [ sql, values ] = this.queryBuilder.build({
         type: 'update',
         columns: this.columns,
         table: this.table,
         values: partial,
         where: selector,
         limit,
      });

      const { affectedRows } = await connection.query( sql, values );

      // TODO : update/insert relations

      if (affectedRows > 0) {
         return this._find(connection, selector, options);
      } else {
         return [];
      }
   }


   async _insertOrUpdateRelations(connection, collection, keyPath, doc, newId, includeKeys) {
      const relations = collection.relations;
      let sql, values, result;

      if (!relations) return;

      for (const relKey in relations) {
         if (relKey in doc) {
            const relation = relations[relKey];
            const nextCollection = this.provider.collections[relation.collection];
           
            if (!nextCollection) {
               throw Error(`Unknown collection ${relation.collection} from ${this.name}`);
            }

            const nextQueryBuilder = nextCollection.queryBuilder;
            const nextPrimaryKey = nextCollection.primaryKey;
            const nextForeignKey = relation.foreignKey || collection.primaryKeyPath;
            const nextDocs = Array.isArray(doc[relKey]) ? doc[relKey] : [ doc[relKey] ];
            const nextKeyPath = keyPath.concat([relKey]);
            
            // add including keys...
            includeKeys[nextKeyPath.join('.')] = 1;
            
            const processNext = async nextConnection => {
               for (let nextDoc of nextDocs) {
                  if (!nextDoc) continue;

                  let nextId = null;

                  if (nextPrimaryKey in nextDoc && nextDoc[nextPrimaryKey] !== DEFAULT_INSERT_VALUE) {
                     const nextDocId = nextDoc[nextPrimaryKey];

                     nextDoc = { ...nextDoc };  // copy
                     delete nextDoc[nextPrimaryKey];  // remove primary key from doc

                     // if REPLACE queries are supported, otherwise use update 
                     [ sql, values ] = nextQueryBuilder.build({
                        type: nextQueryBuilder.types.upsert ? 'upsert' : 'update',
                        columns: nextCollection.columns,
                        table: nextCollection.table,
                        values: { ...nextDoc, [nextForeignKey]: newId },
                        where: { [nextPrimaryKey]: nextDocId },
                     });

                     nextId = nextDocId;
                  } else {
                     [ sql, values ] = nextQueryBuilder.build({
                        type: 'insert',
                        columns: nextCollection.columns,
                        table: nextCollection.table,
                        values: [ { ...nextDoc, [nextPrimaryKey]: DEFAULT_INSERT_VALUE, [nextForeignKey]: newId } ],
                     });
                  }

                  result = await nextConnection.query( sql, values );

                  if (!nextId && result.insertId) {
                     nextId = result.insertId;
                  }

                  await this._insertOrUpdateRelations(nextConnection, nextCollection, nextKeyPath, nextDoc, nextId, includeKeys);
               }
            };

            if (this.provider === nextCollection.provider) {
               await processNext(connection);
            } else {
               await nextCollection.provider.getConnection(processNext);
            }
         }
      }
   }

   async _delete(connection, selector, options) {
      const {
         limit
      } = options || {};

      const [ sql, values ] = this.queryBuilder.build({
         type: 'delete',
         table: this.table,
         where: selector,
         limit,
      });

      const { affectedRows } = await connection.query( sql, values );

      return affectedRows;
   }



   /**
   Find rows from the database using the specified selector and options

   @param {Object} selector
   @param {Object} options  (optional)
   @return {Array}
   */
   async find(selector, options) {
      return this.provider.getConnection(async connection => this._find(connection, selector, options));
   }


   /**
   Find the first row matching the database using the specified selector and options

   @param {Object} selector
   @param {Object} options  (optional)
   @return {Object}
   */
   async findOne(selector, options) {
      options = options || {};
      options.limit = 1;

      return this.find(selector, options).then(rows => rows && rows.length ? rows[0] : null);
   }


   /**
   Count the number of rows matching the given selector

   @param {Object} selector
   @return {Number}
   */
   async count(selector) {
      return this.provider.getConnection(async connection => {
         const [ sql, values ] = this.queryBuilder.build({
            type: 'select',
            columns: {
               count: { type: 'expression', content: 'COUNT(*)' }
            },
            table: this.table,
            where: selector
         });

         const { rows } = await connection.query(sql, values);

         return rows.length ? rows[0].count : 0;
      });
   }


   /**
   Insert multiple rows in the database and return a new array of inserted documents

   @param {Array} documents
   @return {Array}
   */
   async insert(documents) {
      if (!Array.isArray(documents)) {
         throw Error('Documents must be an array');
      } else if (!documents.length) {
         throw Error('No document to update');
      } else if (documents.some(doc => !doc || !Object.keys(doc).length)) {
         throw Error('Cannot insert empty document');
      }

      return this.provider.getConnection(async connection => this._insert(connection, documents, true));
   }


   /**
   Insert a single row in the database and return the updated document

   @param {Object} document
   @return {Object}
   */
   async insertOne(document) {
      if (!document || (typeof document !== 'object')) {
         throw Error('Document must be an object');
      }

      return this.insert([{ ...document }]).then(documents => documents[0]);
   }


   /**
   Update every rows matching the given selector with the partial document and return them

   @param {Object} selector
   @param {Object} partial
   @param {Object} options  (optional)
   @return {Array}
   */
   async update(selector, partial, options) {
      return this.provider.getConnection(async connection => this._update(connection, selector, partial, options));
   }


   /**
   Update the first row matching the given selector with the partial document and return it

   @param {Object} selector
   @param {Object} partial
   @param {Object} options  (optional)
   @return {Object}
   */
   async updateOne(selector, partial, options) {
      options = options || {};
      options.limit = 1;

      return this.update(selector, partial, options).then(documents => documents.length ? documents[0] : null);
   }


   /**
   Delete all rows matching the given selector and options and return the number of
   affected rows

   @param {Object} selector
   @param {Object} options  (optional)
   @return {Number}
   */
   async delete(selector, options) {
      return this.provider.getConnection(async connection => this._delete(connection, selector, options));
   }


   /**
   Delete the first row matching the given selector and options and return the true if successful, false otherwise

   @param {Object} selector
   @param {Object} options  (optional)
   @return {Boolean}
   */
   async deleteOne(selector, options) {
      options = options || {};
      options.limit = 1;

      return this.delete(selector, options).then(deleted => deleted > 0);
   }

}


/**
 * Check that every relations (if defined) are valid
 */
const validRelations = relations => (relations === undefined) || (relations === null) || Object.keys(relations).every(relation => relations[relation] && relations[relation].collection && (typeof relations[relation].collection === 'string'));



export { Collection };