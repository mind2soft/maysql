import { Collection } from './collection';
import { QueryBuilder } from './query';


/**
 * Provides an interface to a database. This class is not responsible to maintain
 * the database, therefore no synchronization is made if not through the queries
 * performed by the user.
 * 
 * The method getConnection(handler) receives a single argument, an async function,
 * which will in turn receive a single connection argument. The provider's
 * responsibility is to provide a standard connection interface, such as defined
 * by DatabaseConnection
 */
class DatabaseProvider {

   collections = {};


   constructor() {
      if (this.constructor === DatabaseProvider) {
         throw Error('Cannot create instance of this class directly');
      }
   }


   /**
    * Execute the given function handler inside a connection context.
    * The handler will receive a DatabaseConnection instance, which
    * should not be cached or reused after the function returns.
    * 
    * @param {Functon} handler 
    * @return {any}
    */
   async getConnection(handler) {
      throw Error('Not implemented');
   }

   /**
    * Return the QueryBuilder instance for this provider
    * 
    * @return {QueryBuilder}
    */
   getQueryBuilder() {
      throw Error('Not implemented');
   }


   /**
    * Create a new collection associated with this provider and return it
    * 
    * @param {String} name 
    * @param {Object} columns 
    * @param {Object} options 
    * @return {Collection}
    */
   addCollection(name, columns, options) {
      checkCollectionName(name, this);

      options = options || {};
      options.provider = this;

      const collection = new Collection(name, columns, options);
      this.collections[name] = collection;
      this[name] = collection;
      return collection;
   }

   /**
    * Remove a collection from this provider
    * 
    * @param {String} name 
    */
   removeCollection(name) {
      if (name in this.collections) {
         const collection = this.collections[name];
         collection.provider = null;  // reset provider from this collection

         delete this.collections[name];
         delete this[name];
      }
   }

}


/**
 * Database connection interface
 */
class DatabaseConnection {
   constructor() {
      if (this.constructor === DatabaseConnection) {
         throw Error('Cannot create instance of this class directly');
      }
   }

   /**
    * Perform a query on the database and return a standardized value :
    * 
    * {
    *   rows: Array
    *   affectedRows: Number
    *   changedRows: Number
    *   insertId: Number
    * }
    * 
    * Where:
    * - `rows`          an array of returned results
    * - `affectedRows`  the number of rows affected (e.g. rows found)
    * - `changedRows`   the number of rows that have been changed by the query (may be equal to affectedRows)
    * - `insertId`      if there were inserts, the first insert id value
    * 
    * @param {String} sql 
    * @param {any} values 
    * @return {Object}
    */
   async query(sql, values) {
      throw Error('Not implemented');
   }
}



const checkCollectionName = (name, inst) => {
   if (!name || (name in inst) || (typeof name !== 'string')) {
      throw Error('Invalid collection name : ' + name);
   }
}



export { DatabaseProvider, DatabaseConnection };