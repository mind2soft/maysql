# MaySQL

NoSQL adapter for SQL database, it may just be SQL


## Usage


```js
import DB from 'maysql/mysql';
import mysql from 'mysql2';
import connectionConfig from './mysql.config.json';



const db = new DB(mysql, connectionConfig);

const userCollection = db.addCollection('users', {
   'id': Number,
   'username': String,
   'password': String,
   'firstName': String,
   'lastName': String,
   'lastConnection': Date,
}, {
   table: 'usr_users',
   primaryKey: 'id',
   provider: connectionProvider,
   relations: {
     projects: { collection: 'projects', hasMany: true, key: 'id', foreign: 'user_id', selector: { active: true } }
   }
});



const proejctCollection = db.addCollection('projects', {
  '@id': Number,      // primaryKey may also be specified this way
  'user_id': Number,
  'name': String,
  'description': String,
  'active': Boolean
}, {
  table: 'projects',
  provider: connectionProvider,
  relations: {
    owner: { collection: 'users'/*, hasMany: false*/, key: 'user_id', /*, foreign: 'id',*/ }
  }
});


const users = await userCollection.find({
   username: 'john.doe',
   active: { $eq: true }
});
// Will execute: SELECT * FROM usr_users WHERE username = 'john.doe' AND active = TRUE;

const projects = await projectCollection.findOne({
  id: 123
}, {
  fields: { owner: 1 }  // include relations
});
// Will execute: SELECT tbl1.*, tbl2.* FROM projects tbl1 LEFT JOIN users tbl2 ON tbl2.id = tbl1.user_id WHERE tbl1.id = 123



```


## Dialects

Each dialect is separated in their own packages in order to help code optimization.

* MySQL - `maysql/mysql`
* PostgreSQL - `maysql/pgsql` *- not implemented*
* MSSQL - `mysql/mssql` *- not implemented*



## API


### db.addCollection(name, template, options)

Create a new collection. This does not create a table, but defines a table as a collection.

* **name** *{String}* 
  the name for this collection, will be made available via `db[name]` or `db.collections[name]`
* **template** *{String}* 
  the document template defines the field specifications
* **options.table** *{String}* 
  the table name
* **options.provider** *{AsyncFunction}* 
  provides the database connection to execute queries. The provider function will receive a single argument, another async
  function, receiving the connection to the database, which will expect the connection to be closed or released upon return
  (the callback will not keep a copy of the connection when it returns)
* **options.relations** *{Object}
  *not implemented*

**NOTE** 
the connection provider is an asynchronous function accepting a single asynchronous function as argument. This provider
is responsible to establish a connection, then cleanup afterwards if necessary. The provider will be called once for
collection method called. The collection does n ot keep a reference to the connection once the queries have completed.


### async collection.find(selector[, options]) : Array

Perform a `SELECT ...` statement on the underlaying table. The method returns a list of matching rows, or
an empty array if no match were found.

* **selector** *{Object}* 
  the query filter, may be an empty object
* **options.fields** *{Object}* 
  the fields to return from the query
* **options.limit** *{Number}* 
  the maximum number of documents to return
* **options.skip** *{Number}* 
  offset the returned collection


### async collection.findOne(selector[, options]) : Object

Perform a `SELECT ... LIMIT 1` statement on the underlaying table. The method returns a single `Object`, or
null if no match was found.

* **selector** *{Object}* 
  the query filter, may be an empty object
* **options.fields** *{Object}* 
  the fields to return from the query
* **options.skip** *{Number}* 
  offset the returned collection


### async collection.count(selector) : Number

Perform a `SELECT COUNT(*)` statement on the underlaying table. The method returns a numeric value 
representing the total number of matches for the given selector.

* **selector** *{Object}* 
  the query filter, may be an empty object


### async collection.insert(documents) : Array

Insert multiple new rows in the underlaying table. The method returns the same documents updated with
the primary key value (ex: `id`).

* **documents** *{Array}*
  a list of documents to insert


### async collection.insertOne(document) : Object

Insert a single row in the underlaying table. The method returns the document updated with the primary
key value (ex: `id`).

* **document** *{Object}*
  the document to insert; the fields from the collection template will be collected from the documents,
  any other fields in the document will be ignored.


### async collection.update(selector, partial[, options]) : Array

Update multiple rows in the underlaying table matching the given selector using the specified partial document.
The method returns all the documents that have been updated, or an empty list if no match was found.

* **selector** *{Object}*
  the query filter, may be an empty object
* **partial** *{Object}*
  the partial document specifying the fields to update
* **options.limit** *{Number}*
  the number of documents to update


### async collection.updateOne(selector, partial[, options]) : Object

Update a single row in the underlaying table matching the given selector using the specified partial document.
The method returns the updated document, or null if no match was found.

If the option `upsert: true` is specified and no document was found, a new one is created.

* **selector** *{Object}*
  the query filter, may be an empty object
* **partial** *{Object}*
  the partial document specifying the fields to update
* **options.upsert** *{Boolean}*
  if no match is found, create a new document



### async collection.delete(selector)

Perform a `DELETE FROM` statement on the underlaying table.

* **selector** *{Object}* 
  the query filter, may be an empty object



### async collection.deleteOne(selector)

Perform a `DELETE FROM ... LIMIT 1` statement on the underlaying table.

* **selector** *{Object}* 
  the query filter, may be an empty object

