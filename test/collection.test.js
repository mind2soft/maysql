import { Collection } from '../src/collection';
import { DatabaseProvider } from '../src/provider';
import { QueryBuilder } from '../src/query';
import { SelectorParser } from '../src/selector';


describe('Testing Collection', () => {

   class MockDatabaseProvider extends DatabaseProvider {

      constructor(options) {
         super();
         this.options = options || {};
      }

      async getConnection(handler) {
         let queryCount = 0;
         return handler({
            query: async (sql, values) => {
               queryCount++;

               if (!this.options.queries) throw Error(`#${queryCount} Unexpected call : query(${JSON.stringify(sql)}, ${JSON.stringify(values)})`);
               if (!this.options.queries.length) throw Error(`#${queryCount} Too many calls : query(${JSON.stringify(sql)}, ${JSON.stringify(values)})`);

               const { 
                  sql: expectedSql,
                  values: expectedValues,
                  result
               } = this.options.queries.shift();
               
               expect( sql ).toBe( expectedSql );
               expect( values ).toEqual( expectedValues );
         
               return result;
            },
         });
      }
   
      getQueryBuilder() {
         return new QueryBuilder({ selectorParser: new SelectorParser() });
      }
   }

   const linkProviders = (...collections) => {
      for (const a of collections) {
         for (const b of collections) {
            a.provider.collections[b.name] = b;
            a.provider[b.name] = b;
         }
      }
   };


   describe('instance', () => {

      it('should create instance', () => {
         const coll = new Collection('test', {
            '@foo': String,
            'bar': Number,
            'buz': Boolean
         }, {
            table: 'tbl_test',
            provider: new MockDatabaseProvider()
         });

         expect( coll.name ).toBe('test');
         expect( coll.table ).toBe('tbl_test');
         expect( coll.columns ).toEqual({ foo: true, bar: true, buz: true });
         expect( coll.primaryKey ).toBe('foo');
      });

      it('should not create instance if invalid collection name', () => {
         expect(() => new Collection()).toThrow();
         
         [
            undefined, null, 0, 1, () => {}, {}, [], new Date(), /./,  ''
         ].forEach(name => expect(() => new Collection(name)).toThrow());
      });

      it('should not create instance if invalid columns', () => {
         expect(() => new Collection('test')).toThrow();

         [
            undefined, null, 0, 1, () => {}, {}, [], new Date(), /./,  ''
         ].forEach(columns => expect(() => new Collection('test', columns)).toThrow());
      });

      it('should not create instance if invalid table', () => {
         expect(() => new Collection('test', { foo: String })).toThrow();

         [
            undefined, null, 0, 1, () => {}, {}, [], new Date(), /./,  ''
         ].forEach(table => expect(() => new Collection('test', { '@foo': String }, { table })).toThrow());
      });

      it('should not create instance if invalid provider', () => {
         expect(() => new Collection('test', { '@foo': String }, { table: 'test' })).toThrow();

         [
            undefined, null, 0, 1, () => {}, {}, [], new Date(), /./,  ''
         ].forEach(provider => expect(() => new Collection('test', { '@foo': String }, { table: 'test', provider })).toThrow());
      });

      it('should not create instance if no primary key', () => {
         const provider = new MockDatabaseProvider();

         expect(() => new Collection('test', { foo: String }, { table: 'test', provider  })).toThrow();
         expect(() => new Collection('test', { '@foo': String }, { table: 'test', provider })).not.toThrow();
      });

      it('should not create instance if invalid primary', () => {
         const provider = new MockDatabaseProvider();

         [
            undefined, null, 0, 1, () => {}, {}, [], new Date(), /./,  ''
         ].forEach(primaryKey => expect(() => new Collection('test', { foo: String }, { table: 'test', primaryKey, provider })).toThrow());
      });

      it('should not create instance if invalid column type', () => {
         const provider = new MockDatabaseProvider();
         const InvalidType = '__INVALID_TYPE';

         expect(() => new Collection('test', { '@foo': InvalidType }, { table: 'test', provider })).toThrow();
      });

      it('should not create instance if conflicting primary keys', () => {
         const provider = new MockDatabaseProvider();

         expect(() => new Collection('test', { '@foo': String, 'bar': String }, { table: 'test', primaryKey: 'bar', provider })).toThrow();
      });

      it('should not create instance if primary key is not in columns', () => {
         const provider = new MockDatabaseProvider();

         expect(() => new Collection('test', { 
            foo: String,
         }, {
            table: 'test',
            primaryKey: 'missing',
            provider
         })).toThrow();
      });

      it('should not create with invalid relations', () => {
         const provider = new MockDatabaseProvider();

         [
            0, 1, () => {}, {}, [], new Date(), /./,  ''
         ].forEach(collection => expect(() => new Collection('test', { 
            foo: String,
         }, {
            table: 'test',
            primaryKey: 'foo',
            relations: { a: collection },
            provider
         })).toThrow());
      });

   });


   describe('find / findOne', () => {

      it('should find documents', async () => {
         const provider = new MockDatabaseProvider({
            queries: [{
               sql: 'SELECT tbl1.id AS `id`, tbl1.foo AS `foo`, tbl1.bar AS `bar` FROM tbl_test tbl1 WHERE tbl1.foo = $1',
               values: [ 'hello' ],
               result: { rows: [{ id: 1, foo: 'hello', bar: 'a' }, { id: 2, foo: 'hello', bar: 'b' }] } 
            }]
         });

         const coll = new Collection('test', {
            '@id': Number,
            foo: String,
            bar: Number,
         }, {
            table: 'tbl_test',
            provider
         });
   
         const docs = await coll.find({ foo: 'hello' });

         expect( docs ).toEqual([ { id: 1, foo: 'hello', bar: 'a' }, { id: 2, foo: 'hello', bar: 'b' } ]);
      });

      it('should find one document', async () => {
         const provider = new MockDatabaseProvider({
            queries: [{
               sql: 'SELECT tbl1.id AS `id`, tbl1.foo AS `foo`, tbl1.bar AS `bar` FROM tbl_test tbl1 WHERE tbl1.foo = $1 LIMIT $2',
               values: [ 'hello', 1 ],
               result: { rows: [{ id: 1, foo: 'hello', bar: 'a' }] },
            }]
         });

         const coll = new Collection('test', {
            '@id': Number,
            foo: String,
            bar: Number,
         }, {
            table: 'tbl_test',
            provider
         });
   
         const doc = await coll.findOne({ foo: 'hello' });

         expect( doc ).toEqual({ id: 1, foo: 'hello', bar: 'a' });
      });

      it('should find no document', async () => {
         const provider = new MockDatabaseProvider({
            queries: [{
               sql: 'SELECT tbl1.id AS `id`, tbl1.foo AS `foo`, tbl1.bar AS `bar` FROM tbl_test tbl1 WHERE tbl1.foo = $1 LIMIT $2',
               values: [ 'hello', 1 ],
               result: { rows: [] }
            }]
         });

         const coll = new Collection('test', {
            '@id': Number,
            foo: String,
            bar: Number,
         }, {
            table: 'tbl_test',
            provider
         });
   
         const doc = await coll.findOne({ foo: 'hello' });

         expect( doc ).toBe(null);
      });

      it('should count documents', async () => {
         const provider = new MockDatabaseProvider({
            queries: [{
               sql: 'SELECT COUNT(*) AS `count` FROM tbl_test tbl1 WHERE tbl1.foo = $1',
               values: [ 'hello' ],
               result: { rows: [{ count: 2 }] }
            }]
         });

         const coll = new Collection('test', {
            '@id': Number,
            foo: String,
            bar: Number,
         }, {
            table: 'tbl_test',
            provider
         });
   
         const count = await coll.count({ foo: 'hello' });

         expect( count ).toBe( 2 );
      });

      it('should remove fields', async () => {
         const provider = new MockDatabaseProvider({
            queries: [{
               sql: 'SELECT tbl1.id AS `id`, tbl1.bar AS `bar` FROM tbl_test tbl1 WHERE tbl1.foo = $1 LIMIT $2',
               values: [ 'hello', 1 ],
               result: { rows: [{ id: 1, bar: true }] }
            }]
         });

         const coll = new Collection('test', {
            '@id': Number,
            foo: String,
            bar: Number,
         }, {
            table: 'tbl_test',
            provider
         });
   
         const doc = await coll.findOne({ foo: 'hello' }, { fields: { foo: 0 } });

         expect( doc ).toEqual({ id: 1, bar: true });
      });

      it('should remove primary key', async () => {
         const provider = new MockDatabaseProvider({
            queries: [{
               sql: 'SELECT tbl1.id AS `id`, tbl1.aCol AS `aCol` FROM tbl_a tbl1 WHERE tbl1.id = $1 LIMIT $2',
               values: [ 123, 1 ],
               result: { rows: [{ id: 123, aCol: 'aCol' }] }
            }]
         });

         const a = new Collection('a', {
            '@id': Number,
            aCol: String,
         }, {
            table: 'tbl_a',
            provider
         });

         const doc = await a.findOne({ id: 123 }, { fields: { id: 0 } });

         expect( doc ).toEqual({ aCol: 'aCol' });
      });

      it('should find 1:1 relation', async () => {
         const provider = new MockDatabaseProvider({
            queries: [{
               sql: 'SELECT tbl1.id AS `id`, tbl1.aCol AS `aCol`, tbl2.id AS `b.id`, tbl2.bCol AS `b.bCol` FROM tbl_a tbl1 JOIN tbl_b tbl2 ON tbl2.id = tbl1.id WHERE tbl1.id = $1 LIMIT $2',
               values: [ 123, 1 ],
               result: { rows: [{ id: 123, aCol: 'aCol', 'b.id': 321, 'b.bCol': 'bCol' }] }
            }]
         });

         const b = new Collection('b', {
            '@id': Number,
            bCol: String,
         }, {
            table: 'tbl_b',
            provider
         });

         const a = new Collection('a', {
            '@id': Number,
            aCol: String,
         }, {
            table: 'tbl_a',
            relations: {
               b: { collection: 'b', hasMany: false }
            },
            provider
         });

         // hack : this is required for testing
         linkProviders(a, b);
   
         const doc = await a.findOne({ id: 123 }, { fields: { b: 1 } });

         expect( doc ).toEqual({ id: 123, aCol: 'aCol', b: { id: 321, bCol: 'bCol' } });
      });

      it('should find nested 1:1 relations', async () => {
         const provider = new MockDatabaseProvider({
            queries: [{
               sql: 'SELECT tbl1.id AS `id`, tbl1.aCol AS `aCol`, tbl2.id AS `b.id`, tbl2.bCol AS `b.bCol`, tbl3.id AS `b.c.id`, tbl3.cCol AS `b.c.cCol` FROM tbl_a tbl1 JOIN tbl_b tbl2 ON tbl2.id = tbl1.id JOIN tbl_c tbl3 ON tbl3.id = tbl2.id WHERE tbl1.id = $1 LIMIT $2',
               values: [ 123, 1 ],
               result: { rows: [{ id: 123, aCol: 'aCol', 'b.id': 41, 'b.bCol': 'bCol', 'b.c.id': 72, 'b.c.cCol': 'cCol' }] }
            }]
         });

         const c = new Collection('c', {
            '@id': Number,
            cCol: String,
         }, {
            table: 'tbl_c',
            provider
         });

         const b = new Collection('b', {
            '@id': Number,
            bCol: String,
         }, {
            table: 'tbl_b',
            relations: {
               c: { collection: 'c', hasMany: false, key: 'id', foreign: 'id' }
            },
            provider
         });

         const a = new Collection('a', {
            '@id': Number,
            aCol: String,
         }, {
            table: 'tbl_a',
            relations: {
               b: { collection: 'b', hasMany: false, key: 'id', foreign: 'id' }
            },
            provider
         });

         // hack : this is required for testing
         linkProviders(a, b, c);

         const doc = await a.findOne({ id: 123 }, { fields: { 'b.c': 1 } });

         expect( doc ).toEqual({ id: 123, aCol: 'aCol', b: { id: 41, bCol: 'bCol', c: { id: 72, cCol: 'cCol' }}});
      });

      it('should find 1:n relations', async () => {
         const provider = new MockDatabaseProvider({
            queries: [{
               sql: 'SELECT tbl1.id AS `id`, tbl1.aCol AS `aCol`, tbl2.id AS `b.id`, tbl2.bCol AS `b.bCol` FROM tbl_a tbl1 JOIN tbl_b tbl2 ON tbl2.id = tbl1.id WHERE tbl1.id = $1 LIMIT $2',
               values: [ 123, 1 ],
               result: { rows: [
                  { id: 123, aCol: 'aCol', 'b.id': 41, 'b.bCol': 'bCol_1' },
                  { id: 123, aCol: 'aCol', 'b.id': 42, 'b.bCol': 'bCol_2' }
               ] }
            }]
         });

         const b = new Collection('b', {
            '@id': Number,
            bCol: String,
         }, {
            table: 'tbl_b',
            provider
         });

         const a = new Collection('a', {
            '@id': Number,
            aCol: String,
         }, {
            table: 'tbl_a',
            relations: {
               b: { collection: 'b', hasMany: true }
            },
            provider
         });

         // hack : this is required for testing
         linkProviders(a, b);
   
         const doc = await a.findOne({ id: 123 }, { fields: { b: 1 } });

         expect( doc ).toEqual({ id: 123, aCol: 'aCol', b: [{ id: 41, bCol: 'bCol_1' }, { id: 42, bCol: 'bCol_2'}] });
      });
      
      it('should find nested 1:n relations', async () => {
         const provider = new MockDatabaseProvider({
            queries: [{
               sql: 'SELECT tbl1.id AS `id`, tbl1.aCol AS `aCol`, tbl2.id AS `b.id`, tbl2.bCol AS `b.bCol` FROM tbl_a tbl1 JOIN tbl_b tbl2 ON tbl2.id = tbl1.id WHERE tbl1.id = $1 LIMIT $2',
               values: [ 123, 1 ],
               result: { rows: [
                  { id: 123, aCol: 'aCol', 'b.id': 41, 'b.bCol': 'bCol_1', 'b.c.id': 63, 'b.c.cCol': 'cCol_1', 'b.c.d.id': 82, 'b.c.d.dCol': 'dCol_1' },
                  { id: 123, aCol: 'aCol', 'b.id': 41, 'b.bCol': 'bCol_1', 'b.c.id': 63, 'b.c.cCol': 'cCol_1', 'b.c.d.id': 83, 'b.c.d.dCol': 'dCol_2' },
                  { id: 123, aCol: 'aCol', 'b.id': 41, 'b.bCol': 'bCol_1', 'b.c.id': 63, 'b.c.cCol': 'cCol_1', 'b.c.d.id': 84, 'b.c.d.dCol': 'dCol_3' },
                  { id: 123, aCol: 'aCol', 'b.id': 41, 'b.bCol': 'bCol_1', 'b.c.id': 64, 'b.c.cCol': 'cCol_2', 'b.c.d.id': 85, 'b.c.d.dCol': 'dCol-4' },
                  { id: 123, aCol: 'aCol', 'b.id': 42, 'b.bCol': 'bCol_2', 'b.c.id': null, 'b.c.cCol': null, 'b.c.d.id': null, 'b.c.d.dCol': null }
               ] }
            }]
         });

         const d = new Collection('d', {
            '@id': Number,
            dCol: String,
         }, {
            table: 'tbl_d',
            provider
         });

         const c = new Collection('c', {
            '@id': Number,
            cCol: String,
         }, {
            table: 'tbl_c',
            relations: { d: { collection: 'd', hasMany: true } },
            provider
         });

         const b = new Collection('b', {
            '@id': Number,
            bCol: String,
         }, {
            table: 'tbl_b',
            relations: { c: { collection: 'c', hasMany: false } },
            provider
         });

         const a = new Collection('a', {
            '@id': Number,
            aCol: String,
         }, {
            table: 'tbl_a',
            relations: {
               b: { collection: 'b', hasMany: true },
               d: { collection: 'd', hasMany: false },
            },
            provider
         });

         // hack : this is required for testing
         linkProviders(a, b, c, d);
   
         const doc = await a.findOne({ id: 123 }, { fields: { b: 1 } });

         expect( doc ).toEqual({ id: 123, aCol: 'aCol', b: [{ id: 41, bCol: 'bCol_1' }, { id: 42, bCol: 'bCol_2'}] });
      });

      it('should fail if missing relation', async () => {
         const provider = new MockDatabaseProvider();

         const a = new Collection('a', {
            '@id': Number,
            aCol: String,
         }, {
            table: 'tbl_a',
            relations: {
               b: { collection: 'b', hasMany: false }
            },
            provider
         });

         await expect(a.findOne({ id: 123 }, { fields: { b: 1 } })).rejects.toThrow('Missing collection b');
      });

   });


   describe('insert', () => {

      it('should not insert invalid document', async () => {
         const provider = new MockDatabaseProvider();

         const coll = new Collection('test', {
            '@id': Number,
            foo: String,
            bar: Number,
         }, {
            table: 'tbl_test',
            provider
         });

         const invalid = [
            undefined, null, 0, 1, {}, [], [null], [{}], () => {}, /./, new Date(), '', 'test'
         ];

         for (const doc of invalid) {
            await expect(coll.insert(doc)).rejects.toThrow();
            await expect(coll.insertOne(doc)).rejects.toThrow();
         }
      });

      it('should insert multiple documents', async () => {
         const provider = new MockDatabaseProvider({
            queries: [{
               sql: 'INSERT INTO tbl_test (id,foo,bar) VALUES (DEFAULT,$1,$2)',
               values: [ 'banana', true ],
               result: { affectedRows: 1, insertId: 1 }
            }, {
               sql: 'INSERT INTO tbl_test (id,foo,bar) VALUES (DEFAULT,$1,$2)',
               values: [ 'apple', false ],
               result: { affectedRows: 1, insertId: 2 }
            }, {
               sql: 'SELECT tbl1.id AS `id`, tbl1.foo AS `foo`, tbl1.bar AS `bar` FROM tbl_test tbl1 WHERE tbl1.id IN ($1,$2)',
               values: [ 1, 2 ],
               result: { rows: [{ id: 1, foo: 'banana', bar: true }, { id: 2, foo: 'apple', bar: false }] }
            }]
         });

         const coll = new Collection('test', {
            '@id': Number,
            foo: String,
            bar: Number,
         }, {
            table: 'tbl_test',
            provider
         });

         const documents = await coll.insert([ { foo: 'banana', bar: true }, { foo: 'apple', bar: false }]);

         expect( documents ).toEqual([{ id: 1, foo: 'banana', bar: true }, { id: 2, foo: 'apple', bar: false }]);
      });

      it('should insert single document', async () => {
         const provider = new MockDatabaseProvider({
            queries: [{
               sql: 'INSERT INTO tbl_test (id,foo,bar) VALUES (DEFAULT,$1,$2)',
               values: [ 'banana', true ],
               result: { affectedRows: 1, insertId: 7 }
            }, {
               sql: 'SELECT tbl1.id AS `id`, tbl1.foo AS `foo`, tbl1.bar AS `bar` FROM tbl_test tbl1 WHERE tbl1.id IN ($1)',
               values: [ 7 ],
               result: { rows: [{ id: 7, foo: 'banana', bar: true }, { id: 2, foo: 'apple', bar: false }] }
            }]
         });

         const coll = new Collection('test', {
            '@id': Number,
            foo: String,
            bar: Number,
         }, {
            table: 'tbl_test',
            provider
         });

         const document = await coll.insertOne({ foo: 'banana', bar: true });

         expect( document ).toEqual({ id: 7, foo: 'banana', bar: true });
      });

      it('should insert 1:1 relation', async () => {
         const provider = new MockDatabaseProvider({
            queries: [{
               sql: 'INSERT INTO tbl_a (id,aCol) VALUES (DEFAULT,$1)',
               values: [ 'aCol' ],
               result: { affectedRows: 1, insertId: 123 }
            }, {
               sql: 'INSERT INTO tbl_b (id,bCol) VALUES (DEFAULT,$1)',
               values: [ 'bCol' ],
               result: { affectedRows: 1, insertId: 321 }
            }, {
               sql: 'SELECT tbl1.id AS `id`, tbl1.aCol AS `aCol`, tbl2.id AS `b.id`, tbl2.bCol AS `b.bCol` FROM tbl_a tbl1 JOIN tbl_b tbl2 ON tbl2.id = tbl1.id WHERE tbl1.id IN ($1)',
               values: [ 123 ],
               result: { rows: [{ id: 123, aCol: 'aCol', 'b.id': 321, 'b.bCol': 'bCol' }] }
            }]
         });

         const b = new Collection('b', {
            '@id': Number,
            bCol: String,
         }, {
            table: 'tbl_b',
            provider
         });

         const a = new Collection('a', {
            '@id': Number,
            aCol: String,
         }, {
            table: 'tbl_a',
            relations: {
               b: { collection: 'b', hasMany: false }
            },
            provider
         });

         // hack : this is required for testing
         linkProviders(a, b);

         const doc = await a.insertOne({ aCol: 'aCol', b: { bCol: 'bCol' } });

         expect( doc ).toEqual({ id: 123, aCol: 'aCol', b: { id: 321, bCol: 'bCol' } });
      });

      it('should insert nested 1:1 relations', async () => {
         const provider = new MockDatabaseProvider({
            queries: [{
               sql: 'INSERT INTO tbl_a (id,aCol) VALUES (DEFAULT,$1)',
               values: [ 'aCol' ],
               result: { affectedRows: 1, insertId: 123 }
            }, {
               sql: 'INSERT INTO tbl_b (id,bCol) VALUES (DEFAULT,$1)',
               values: [ 'bCol' ],
               result: { affectedRows: 1, insertId: 321 }
            }, {
               sql: 'INSERT INTO tbl_c (id,cCol) VALUES (DEFAULT,$1)',
               values: [ 'cCol-1' ],
               result: { affectedRows: 1, insertId: 321 }
            }, {
               sql: 'UPDATE tbl_c SET cCol = $1 WHERE id = $2',
               values: [ 'cCol-2', 402 ],
               result: { affectedRows: 1, changedRows: 1 }
            }, {
               sql: 'SELECT tbl1.id AS `id`, tbl1.aCol AS `aCol`, tbl2.id AS `b.id`, tbl2.bCol AS `b.bCol`, tbl3.id AS `b.c.id`, tbl3.cCol AS `b.c.cCol` FROM tbl_a tbl1 JOIN tbl_b tbl2 ON tbl2.id = tbl1.id JOIN tbl_c tbl3 ON tbl3.id = tbl2.id WHERE tbl1.id IN ($1)',
               values: [ 123 ],
               result: { rows: [
                  { id: 123, aCol: 'aCol', 'b.id': 321, 'b.bCol': 'bCol', 'b.c.id': 401, 'b.c.cCol': 'cCol-1' },
                  { id: 123, aCol: 'aCol', 'b.id': 321, 'b.bCol': 'bCol', 'b.c.id': 402, 'b.c.cCol': 'cCol-2' },
               ] }
            }]
         });

         const c = new Collection('c', {
            '@id': Number,
            cCol: String,
         }, {
            table: 'tbl_c',
            provider
         });

         const b = new Collection('b', {
            '@id': Number,
            bCol: String,
         }, {
            table: 'tbl_b',
            relations: {
               c: { collection: 'c', hasMany: true }
            },
            provider
         });

         const a = new Collection('a', {
            '@id': Number,
            aCol: String,
         }, {
            table: 'tbl_a',
            relations: {
               b: { collection: 'b', hasMany: false }
            },
            provider
         });

         // hack : this is required for testing
         linkProviders(a, b, c);

         const doc = await a.insertOne({ aCol: 'aCol', b: { bCol: 'bCol', c: [ { cCol: 'cCol-1', }, { id: 402, cCol: 'cCol-2' }] } });

         expect( doc ).toEqual({ id: 123, aCol: 'aCol', b: { id: 321, bCol: 'bCol', c: [{ id: 401, cCol: 'cCol-1' }, { id: 402, cCol: 'cCol-2' }] } });
      });

      it('should fail if missing relation', async () => {
         const provider = new MockDatabaseProvider({
            queries: [{
               sql: 'INSERT INTO tbl_a (id,aCol) VALUES (DEFAULT,$1)',
               values: [ 'aCol' ],
               result: { affectedRows: 1, insertId: 1 }
            }]
         });

         const a = new Collection('a', {
            '@id': Number,
            aCol: String,
         }, {
            table: 'tbl_a',
            relations: {
               b: { collection: 'b', hasMany: false }
            },
            provider
         });

         await expect(a.insertOne({ aCol: 'aCol', b: { bCol: 'bCol' } })).rejects.toThrow('Unknown collection b');
      });

   });


   describe('update', () => {

      it('should update documents', async () => {
         const provider = new MockDatabaseProvider({
            queries: [{
               sql: 'UPDATE tbl_test SET bar = $1 WHERE bar = $2',
               values: [ true, false ],
               result: { affectedRows: 2, changedRows: 2 }
            }, {
               sql: 'SELECT tbl1.id AS `id`, tbl1.foo AS `foo`, tbl1.bar AS `bar` FROM tbl_test tbl1 WHERE tbl1.bar = $1',
               values: [ false ],
               result: { rows: [{ id: 7, foo: 'banana', bar: true }, { id: 8, foo: 'apple', bar: true }] }
            }]
         });

         const coll = new Collection('test', {
            '@id': Number,
            foo: String,
            bar: Number,
         }, {
            table: 'tbl_test',
            provider
         });

         const documents = await coll.update({ bar: false }, { bar: true });

         expect( documents ).toEqual([{ id: 7, foo: 'banana', bar: true }, { id: 8, foo: 'apple', bar: true }]);
      });

      it('should return empty array if no update', async () => {
         const provider = new MockDatabaseProvider({
            queries: [{
               sql: 'UPDATE tbl_test SET bar = $1 WHERE bar = $2',
               values: [ true, false ],
               result: { affectedRows: 0, changedRows: 0 }
            }]
         });

         const coll = new Collection('test', {
            '@id': Number,
            foo: String,
            bar: Number,
         }, {
            table: 'tbl_test',
            provider
         });

         const documents = await coll.update({ bar: false }, { bar: true });

         expect( documents ).toHaveLength(0);
      });

      it('should update single document', async () => {
         const provider = new MockDatabaseProvider({
            queries: [{
               sql: 'UPDATE tbl_test SET bar = $1 WHERE bar = $2 LIMIT $3',
               values: [ true, false, 1 ],
               result: { affectedRows: 1 }
            }, {
               sql: 'SELECT tbl1.id AS `id`, tbl1.foo AS `foo`, tbl1.bar AS `bar` FROM tbl_test tbl1 WHERE tbl1.bar = $1 LIMIT $2',
               values: [ false, 1 ],
               result: { rows: [{ id: 7, foo: 'banana', bar: true }] }
            }]
         });

         const coll = new Collection('test', {
            '@id': Number,
            foo: String,
            bar: Number,
         }, {
            table: 'tbl_test',
            provider
         });

         const document = await coll.updateOne({ bar: false }, { bar: true });

         expect( document ).toEqual({ id: 7, foo: 'banana', bar: true }, { id: 8, foo: 'apple', bar: true });
      });

      it('should return null if nothing to update on single document', async () => {
         const provider = new MockDatabaseProvider({
            queries: [{
               sql: 'UPDATE tbl_test SET bar = $1 WHERE bar = $2 LIMIT $3',
               values: [ true, false, 1 ],
               result: { affectedRows: 0 }
            }]
         });

         const coll = new Collection('test', {
            '@id': Number,
            foo: String,
            bar: Number,
         }, {
            table: 'tbl_test',
            provider
         });

         const document = await coll.updateOne({ bar: false }, { bar: true });

         expect( document ).toBeNull();
      });

      test.todo('should update 1:1 relation');

      test.todo('should update nested 1:1 relations');

      test.todo('should update 1:n relations');
      
      test.todo('should update nested 1:n relations');

   });


   describe('delete', () => {

      it('should delete documents', async () => {
         const provider = new MockDatabaseProvider({
            queries: [{
               sql: 'DELETE FROM tbl_test WHERE bar = $1',
               values: [ false ],
               result: { affectedRows: 2 }
            }]
         });

         const coll = new Collection('test', {
            '@id': Number,
            foo: String,
            bar: Number,
         }, {
            table: 'tbl_test',
            provider
         });

         const count = await coll.delete({ bar: false });

         expect( count ).toBe( 2 );
      });

      it('should delete one document', async () => {
         const provider = new MockDatabaseProvider({
            queries: [{
               sql: 'DELETE FROM tbl_test WHERE bar = $1 LIMIT $2',
               values: [ false, 1 ],
               result: { affectedRows: 1 }
            }]
         });

         const coll = new Collection('test', {
            '@id': Number,
            foo: String,
            bar: Number,
         }, {
            table: 'tbl_test',
            provider
         });

         const deleted = await coll.deleteOne({ bar: false });

         expect( deleted ).toBe(true);
      });

   });

});