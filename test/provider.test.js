import { DatabaseProvider, DatabaseConnection } from '../src/provider';



describe('Testing providers', () => {
   
   describe('DatabaseProvider', () => {

      class MockDatabaseProvider extends DatabaseProvider {}


      it('should not create direct instances', () => {
         expect(() => new DatabaseProvider()).toThrow();
      });

      it('should define interface', async () => {

         const provider = new MockDatabaseProvider();

         expect( provider ).toHaveProperty('getConnection');
         expect( provider ).toHaveProperty('getQueryBuilder');
         expect( provider ).toHaveProperty('addCollection');
         expect( provider ).toHaveProperty('removeCollection');

         await expect(provider.getConnection()).rejects.toThrow('Not implemented');
         expect(() => provider.getQueryBuilder()).toThrow('Not implemented');
      });


      describe('collections', () => {

         class MockDatabaseProvider extends DatabaseProvider {
            getQueryBuilder() {
               return null;
            }
         }

         it('should create collection', () => {

            const provider = new MockDatabaseProvider();

            expect( provider ).not.toHaveProperty('test');

            provider.addCollection('test', {
               '@a': String
            }, {
               table: 'test'
            });

            expect( provider ).toHaveProperty('test');
            expect( provider.collections ).toHaveProperty('test');
         });

         it('should remove collection', () => {

            const provider = new MockDatabaseProvider();

            expect( provider ).not.toHaveProperty('test');

            provider.addCollection('test', {
               '@a': String
            }, {
               table: 'test'
            });

            expect( provider ).toHaveProperty('test');

            provider.removeCollection('test');

            expect( provider ).not.toHaveProperty('test');
            expect( provider.collections ).not.toHaveProperty('test');
         });

         it('should not allow invalid collection names', () => {
            
            const provider = new MockDatabaseProvider();

            [
               undefined, null, true, false,
               -1, 0, 1,
               () => {}, {}, [], /./, new Date(),
               '', 'collections', 'addCollection', 'removeCollection',
               'getConnection', 'getQueryBuilder'         
            ].forEach(name => expect(() => provider.addCollection(name, { '@a': String }, { table: 'null' })).toThrow('Invalid collection name : ' + name));
         });

         it('should add collection without options', () => {
            const provider = new MockDatabaseProvider();

            const collection = provider.addCollection('test', { '@id': Number });

            expect( collection.name ).toBe( 'test' );
            expect( collection.table ).toBe( collection.name );
         });

         it('should ignore remocing missing collections', () => {
            const provider = new MockDatabaseProvider();

            expect(() => provider.removeCollection('missing')).not.toThrow();
         });

      });

   });


   describe('DatabaseConnection', () => {

      class MockDatabaseConnection extends DatabaseConnection {}


      it('should not create direct instances', () => {
         expect(() => new DatabaseConnection()).toThrow();
      });
   
      it('should define interface', async () => {

         const connection = new MockDatabaseConnection();

         expect( connection ).toHaveProperty('query');

         await expect(connection.query()).rejects.toThrow('Not implemented');
      });

   });
   

});