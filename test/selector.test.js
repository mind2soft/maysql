import { SelectorParser } from '../src/selector';




describe('Testing abstract selector', () => {


   const testParse = (parser, selector, expectedExpr, expectedParams) => {
      const params = [];
      const where = parser.parse(selector, params);
      expect( where ).toBe(expectedExpr);
      expect( params ).toEqual( expectedParams);
   };

   
   it('should return empty if no key', () => {
      const parser = new SelectorParser();

      [
         undefined, null,
         {},
         [],
         [0], [0, 1],
         [''], ['foo'], ['foo', 'bar'],
         0, 1, 2,
         '', 'test'
      ].forEach(selector => testParse(parser, selector, '', []));
   });


   describe('Do NOT use params', () => {

      const parser = new SelectorParser();
      
      parser.useParams = false;


      describe('$eq / $ne', () => {

         it('should default to $q', () => {
            const d = new Date();

            testParse(parser, { a: 'Hello' },'a = "Hello"', []);
            testParse(parser, { a: 123 },'a = 123', []);
            testParse(parser, { a: null },'a IS NULL', []);
            testParse(parser, { a: d },'a = "' + d.toISOString() + '"', []);
            testParse(parser, { a: true },'a = TRUE', []);
            testParse(parser, { a: false },'a = FALSE', []);
            testParse(parser, { a: 'Hello', b: 123, c: true, d: null },'a = "Hello" AND b = 123 AND c = TRUE AND d IS NULL', []);
            testParse(parser, { a: { b: { c: 'foo' }, d: 123 }},'a.b.c = "foo" AND a.d = 123', []);
         });

         it('should equal', () => {
            testParse(parser, { a: { $eq: 'Hello' } },'a = "Hello"', []);
            testParse(parser, { a: { $eq: 123 } },'a = 123', []);
         });

         it('should equal null', () => {
            [
               null, undefined
            ].forEach(value => testParse(parser, { a: { $eq: value } }, 'a IS NULL', []));
         });

         it('should not equal', () => {
            testParse(parser, { a: { $ne: 'Hello' } }, 'a <> "Hello"', []);
            testParse(parser, { a: { $ne: 123 } }, 'a <> 123', []);
         });

         it('should not equal null', () => {
            [
               null, undefined
            ].forEach(value => testParse(parser, { a: { $ne: value } }, 'a IS NOT NULL', []));
         });

      });


      describe('$gt / $gte', () => {

         it('should be greater than', () => {
            testParse(parser, { a: { $gt: 123 } }, 'a > 123', []);
         });

         it('should be greater than or equal', () => {
            testParse(parser, { a: { $gte: 123 } }, 'a >= 123', []);
         });

      });



      describe('$lt / $lte', () => {

         it('should be less than', () => {
            testParse(parser, { a: { $lt: 123 } }, 'a < 123', []);
         });

         it('should be less than or equal', () => {
            testParse(parser, { a: { $lte: 123 } }, 'a <= 123', []);
         });

      });


      describe('$in / $nin', () => {

         it('should be in', () => {
            testParse(parser, { a: { $in: [ 123 ] } }, 'a IN (123)', []);
            testParse(parser, { a: { $in: [ 123, 456 ] } }, 'a IN (123,456)', []);
         });

         it('should not be in', () => {
            testParse(parser, { a: { $nin: [ 123 ] } }, 'a NOT IN (123)', []);
            testParse(parser, { a: { $nin: [ 123, 456 ] } }, 'a NOT IN (123,456)', []);
         });

         it('should throw if not an array', () => {
            expect(() => testParse(parser, { a: { $in: 123 } })).toThrow();
            expect(() => testParse(parser, { a: { $nin: 123 } })).toThrow();
         });

      });


      describe('$regex', () => {

         it('should use default if using regular expression value', () => {
            testParse(parser, { a: /./ }, 'a REGEXP "."', []);
         });

         it('should use default if using regular expression value', () => {
            testParse(parser, { a: { $ne: /./ } }, 'a NOT REGEXP "."', []);
         });

         it('should use case insensitive', () => {
            testParse(parser, { a: /.ABC/i }, 'LOWER(a) REGEXP ".abc"', []);
            testParse(parser, { a: { $regex: /.ABC/i } }, 'LOWER(a) REGEXP ".abc"', []);
            testParse(parser, { a: { $regex: /.ABC/i, $options: 'i' } }, 'LOWER(a) REGEXP ".abc"', []);
            testParse(parser, { a: { $regex: /.ABC/, $options: 'i' } }, 'LOWER(a) REGEXP ".abc"', []);
         });

         it('should negate', () => {
            testParse(parser, { a: { $ne: /.ABC/i } }, 'LOWER(a) NOT REGEXP ".abc"', []);
            testParse(parser, { a: { $regex: /.ABC/, $negate: true } }, 'a NOT REGEXP ".ABC"', []);
            testParse(parser, { a: { $regex: /.ABC/i, $negate: true } }, 'LOWER(a) NOT REGEXP ".abc"', []);
         });

         it('should negate using $not', () => {
            testParse(parser, { a: { $not: /.ABC/i } }, 'NOT (LOWER(a) REGEXP ".abc")', []);
            testParse(parser, { a: { $not: /.ABC/ } }, 'NOT (a REGEXP ".ABC")', []);
            testParse(parser, { a: { $not: { $regex: /.ABC/i } } }, 'NOT (LOWER(a) REGEXP ".abc")', []);
         });

      });


      describe('$and', () => {

         it('should ignore if no key', () => {
            [
               {},
               [],
               [0], [0, 1],
               [''], ['foo'], ['foo', 'bar'],
               0, 1, 2,
               '', 'test'
            ].forEach(selector => testParse(parser, { $and: selector }, '', []));
         });

         it('should and multiple conditions', () => {
            testParse(parser, { a: { $and: [ 1, 2, 3 ] } }, '(a = 1 AND a = 2 AND a = 3)', []);
         });

      });


      describe('$or', () => {

         it('should ignore if no key', () => {
            [
               {},
               [],
               [0], [0, 1],
               [''], ['foo'], ['foo', 'bar'],
               0, 1, 2,
               '', 'test'
            ].forEach(selector => testParse(parser, { $or: selector }, '', []));
         });

         it('should or multiple conditions', () => {
            testParse(parser, { a: { $or: [ 1, 2, 3 ] } }, '(a = 1 OR a = 2 OR a = 3)', []);
         });

      });


      describe('$or', () => {

         it('should ignore if no key', () => {
            [
               {},
               [],
               [0], [0, 1],
               [''], ['foo'], ['foo', 'bar'],
               0, 1, 2,
               '', 'test'
            ].forEach(selector => testParse(parser, { $or: selector }, '', []));
         });

         it('should exclude multiple conditions', () => {
            testParse(parser, { a: { $nor: [ 1, 2, 3 ] } }, 'NOT (a = 1 OR a = 2 OR a = 3)', []);
         });

      });


      describe('$not', () => {

         it('should negate', () => {
            testParse(parser, { a: { $not: [ { $gt: 2 }, { $lt: 10 } ] } }, 'NOT (a > 2 AND a < 10)', []);
         })

      });
   
   });


   describe('Do use params', () => {

      const parser = new SelectorParser();
      
      parser.useParams = true;


      describe('$eq / $ne', () => {

         it('should default to $q', () => {
            const d = new Date();

            testParse(parser, { a: 'Hello' }, 'a = $1', [ 'Hello' ]);
            testParse(parser, { a: 123 }, 'a = $1', [ 123 ]);
            testParse(parser, { a: null }, 'a IS $1', [ null ]);
            testParse(parser, { a: d }, 'a = $1', [ d ]);
            testParse(parser, { a: true }, 'a = $1', [ true ]);
            testParse(parser, { a: false }, 'a = $1', [ false ]);
            testParse(parser, { a: 'Hello', b: 123, c: true, d: null }, 'a = $1 AND b = $2 AND c = $3 AND d IS $4', [ 'Hello', 123, true, null ]);
            testParse(parser, { a: { b: { c: 'foo' }, d: 123 }}, 'a.b.c = $1 AND a.d = $2', [ 'foo', 123 ]);
         });

         it('should equal', () => {
            testParse(parser, { a: { $eq: 'Hello' } }, 'a = $1', [ 'Hello' ]);
            testParse(parser, { a: { $eq: 123 } }, 'a = $1', [ 123 ]);
         });

         it('should equal null', () => {
            [
               null, undefined
            ].forEach(value => testParse(parser, { a: { $eq: value } }, 'a IS $1', [ null ]));
         });

         it('should not equal', () => {
            testParse(parser, { a: { $ne: 'Hello' } }, 'a <> $1', [ 'Hello' ]);
            testParse(parser, { a: { $ne: 123 } }, 'a <> $1', [ 123 ]);
         });

         it('should not equal null', () => {
            [
               null, undefined
            ].forEach(value => testParse(parser, { a: { $ne: value } }, 'a IS NOT $1', [ null ]));
         });

      });


      describe('$gt / $gte', () => {

         it('should be greater than', () => {
            testParse(parser, { a: { $gt: 123 } }, 'a > $1', [ 123 ]);
         });

         it('should be greater than or equal', () => {
            testParse(parser, { a: { $gte: 123 } }, 'a >= $1', [ 123 ]);
         });

      });



      describe('$lt / $lte', () => {

         it('should be less than', () => {
            testParse(parser, { a: { $lt: 123 } }, 'a < $1', [ 123 ]);
         });

         it('should be less than or equal', () => {
            testParse(parser, { a: { $lte: 123 } }, 'a <= $1', [ 123 ]);
         });

      });


      describe('$in / $nin', () => {

         it('should be in', () => {
            testParse(parser, { a: { $in: [ 123 ] } }, 'a IN ($1)', [ 123 ]);
            testParse(parser, { a: { $in: [ 123, 456 ] } }, 'a IN ($1,$2)', [ 123, 456 ]);
         });

         it('should not be in', () => {
            testParse(parser, { a: { $nin: [ 123 ] } }, 'a NOT IN ($1)', [ 123 ]);
            testParse(parser, { a: { $nin: [ 123, 456 ] } }, 'a NOT IN ($1,$2)', [ 123, 456 ]);
         });

         it('should throw if not an array', () => {
            expect(() => testParse(parser, { a: { $in: 123 } })).toThrow();
            expect(() => testParse(parser, { a: { $nin: 123 } })).toThrow();
         });

      });


      describe('$regex', () => {

         it('should use default if using regular expression value', () => {
            testParse(parser, { a: /./ }, 'a REGEXP $1', [ '.' ]);
         });

         it('should use default if using regular expression value', () => {
            testParse(parser, { a: { $ne: /./ } }, 'a NOT REGEXP $1', [ '.' ]);
         });

         it('should use case insensitive', () => {
            testParse(parser, { a: /.ABC/i }, 'LOWER(a) REGEXP $1', [ '.abc' ]);
            testParse(parser, { a: { $regex: /.ABC/i } }, 'LOWER(a) REGEXP $1', [ '.abc' ]);
            testParse(parser, { a: { $regex: /.ABC/i, $options: 'i' } }, 'LOWER(a) REGEXP $1', [ '.abc' ]);
            testParse(parser, { a: { $regex: /.ABC/, $options: 'i' } }, 'LOWER(a) REGEXP $1', [ '.abc' ]);
         });

         it('should negate', () => {
            testParse(parser, { a: { $ne: /.ABC/i } }, 'LOWER(a) NOT REGEXP $1', [ '.abc' ]);
            testParse(parser, { a: { $regex: /.ABC/, $negate: true } }, 'a NOT REGEXP $1', [ '.ABC' ]);
            testParse(parser, { a: { $regex: /.ABC/i, $negate: true } }, 'LOWER(a) NOT REGEXP $1', [ '.abc' ]);
         });

         it('should negate using $not', () => {
            testParse(parser, { a: { $not: /.ABC/i } }, 'NOT (LOWER(a) REGEXP $1)', [ '.abc' ]);
            testParse(parser, { a: { $not: /.ABC/ } }, 'NOT (a REGEXP $1)', [ '.ABC' ]);
            testParse(parser, { a: { $not: { $regex: /.ABC/i } } }, 'NOT (LOWER(a) REGEXP $1)', [ '.abc' ]);
         });

      });


      describe('$and', () => {

         it('should ignore if no key', () => {
            [
               {},
               [],
               [0], [0, 1],
               [''], ['foo'], ['foo', 'bar'],
               0, 1, 2,
               '', 'test'
            ].forEach(selector => testParse(parser, { $and: selector }, '', []));
         });

         it('should and multiple conditions', () => {
            testParse(parser, { a: { $and: [ 1, 2, 3 ] } }, '(a = $1 AND a = $2 AND a = $3)', [ 1, 2, 3 ]);
         });

      });


      describe('$or', () => {

         it('should ignore if no key', () => {
            [
               {},
               [],
               [0], [0, 1],
               [''], ['foo'], ['foo', 'bar'],
               0, 1, 2,
               '', 'test'
            ].forEach(selector => testParse(parser, { $or: selector }, '', [] ));
         });

         it('should or multiple conditions', () => {
            testParse(parser, { a: { $or: [ 1, 2, 3 ] } }, '(a = $1 OR a = $2 OR a = $3)', [ 1, 2, 3 ]);
         });

      });

      describe('$or', () => {

         it('should ignore if no key', () => {
            [
               {},
               [],
               [0], [0, 1],
               [''], ['foo'], ['foo', 'bar'],
               0, 1, 2,
               '', 'test'
            ].forEach(selector => testParse(parser, { $or: selector }, '', [] ));
         });

         it('should exclude multiple conditions', () => {
            testParse(parser, { a: { $nor: [ 1, 2, 3 ] } }, 'NOT (a = $1 OR a = $2 OR a = $3)', [ 1, 2, 3 ]);
         });

      });

      describe('$not', () => {

         it('should negate', () => {
            testParse(parser, { a: { $not: [ { $gt: 2 }, { $lt: 10 } ] } }, 'NOT (a > $1 AND a < $2)', [ 2, 10 ]);
         });

      });


      describe('Multiple conditions', () => {

         it('should combine two arrays', () => {
            testParse(parser, { a: { $or: [ { $in: [ 'a','b' ] }, { $in: [ 'y', 'z' ] } ] }, b: { $nin: [ 1, 2, 3 ] } }, '(a IN ($1,$2) OR a IN ($3,$4)) AND b NOT IN ($5,$6,$7)', [ 'a', 'b', 'y', 'z', 1, 2, 3 ]);
         });

      });
  
   });

});