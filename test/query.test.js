import { SelectorParser } from '../src/selector';
import { QueryBuilder, DEFAULT_INSERT_VALUE } from '../src/query';



describe('Testing QueryBuilder', () => {


   const queryBuilder = new QueryBuilder({
      selectorParser: new SelectorParser()
   });


   describe('SELECT', () => {
     
      it('should build', () => {
         const [ sql, values ] = queryBuilder.build({
            type: 'select',
            columns: { 
               id: 'id',
               foo: 'foo',
               barAlias: 'bar',
            },
            table: 'tbl_test',
            where: {
               foo: 'hello'
            }
         });

         expect( sql ).toBe('SELECT tbl1.id AS `id`, tbl1.foo AS `foo`, tbl1.bar AS `barAlias` FROM tbl_test tbl1 WHERE tbl1.foo = $1');
         expect( values ).toEqual([ "hello" ]);
      });

      it('should select with limit', () => {
         const [ sql, values ] = queryBuilder.build({
            type: 'select',
            columns: { 
               id: 'id',
               foo: 'foo',
               barAlias: 'bar',
            },
            table: 'tbl_test',
            where: {
               foo: 'hello'
            },
            limit: 10
         });

         expect( sql ).toBe('SELECT tbl1.id AS `id`, tbl1.foo AS `foo`, tbl1.bar AS `barAlias` FROM tbl_test tbl1 WHERE tbl1.foo = $1 LIMIT $2');
         expect( values ).toEqual([ "hello", 10 ]);
      });

      it('should select with expression', () => {
         const [ sql, values ] = queryBuilder.build({
            type: 'select',
            columns: {
               count: { type: 'expression', content: ['COUNT(', { type: 'field', name: 'id' }, ')'] }
            },
            table: 'tbl_test',
            where: {
               active: true
            }
         });

         expect( sql ).toBe('SELECT COUNT(tbl1.id) AS `count` FROM tbl_test tbl1 WHERE tbl1.active = $1');
         expect( values ).toEqual([ true ]);
      });

      it('should select with columsn as select', () => {
         const [ sql, values ] = queryBuilder.build({
            type: 'select',
            columns: {
               id: true,
               foo: {
                  type: 'select',
                  columns: {
                     foo: true
                  },
                  table: { name: 'table_b', as: 'b' },
                  where: {
                     id: { $field: 'id', $table: 'table_a' }
                  }
               }
            },
            table: 'table_a',
            where: {
               foo: 'foo'
            }
         });

         expect( sql ).toBe('SELECT tbl1.id AS `id`, (SELECT b.foo AS `foo` FROM table_b b WHERE b.id = tbl1.id) AS `foo` FROM table_a tbl1 WHERE tbl1.foo = $1');
         expect( values ).toEqual([ 'foo' ]);
      });

   });


   describe('JOIN', () => {

      it('should create simple JOIN', () => {
         const [ sql, values ] = queryBuilder.build({
            type: 'join',
            table: 'tbl_test',
            on: {
               foo: 'hello'
            }
         });

         expect( sql ).toBe('JOIN tbl_test tbl1 ON tbl1.foo = $1');
         expect( values ).toEqual([ "hello" ]);
      });


      it('should SELECT + JOIN auto-naming', () => {
         const [ sql, values ] = queryBuilder.build({
            type: 'select',
            columns: { 
               id: 'id',
               foo: 'bar',
            },
            table: 'tbl_test',
            joins: [
               {
                  type: 'join',
                  join: 'left',
                  columns: {
                     other_id: 'id',
                     other_foo: 'bar'
                  },
                  table: 'tbl_join',
                  on: {
                     other_bar: { $field: 'bar', $table: 'tbl_test' },
                  }
               }
            ],
            where: {
               bar: 'hello'
            },
            limit: 10
         });

         //console.log( sql );

         expect( sql ).toBe('SELECT tbl1.id AS `id`, tbl1.bar AS `foo`, tbl2.id AS `other_id`, tbl2.bar AS `other_foo` FROM tbl_test tbl1 LEFT JOIN tbl_join tbl2 ON tbl2.other_bar = tbl1.bar WHERE tbl1.bar = $1 LIMIT $2');
         expect( values ).toEqual([ "hello", 10 ]);
      });

      it('should SELECT + JOIN specifying table aliases', () => {
         const [ sql, values ] = queryBuilder.build({
            type: 'select',
            columns: { 
               id: 'id',
               foo: 'bar',
            },
            table: { name: 'tbl_test', as: 'test' },
            joins: [
               {
                  type: 'join',
                  join: 'left',
                  columns: {
                     other_id: 'id',
                     other_foo: 'bar'
                  },
                  table: { name: 'tbl_join', as: 'other' },
                  on: {
                     other_bar: { $field: 'bar', $table: 'tbl_test' },
                  }
               }
            ],
            where: {
               bar: 'hello'
            },
            limit: 10
         });

         //console.log( sql );

         expect( sql ).toBe('SELECT test.id AS `id`, test.bar AS `foo`, other.id AS `other_id`, other.bar AS `other_foo` FROM tbl_test test LEFT JOIN tbl_join other ON other.other_bar = test.bar WHERE test.bar = $1 LIMIT $2');
         expect( values ).toEqual([ "hello", 10 ]);
      });

   });


   describe('INSERT', () => {

      it('should insert', () => {
         const [ sql, values ] = queryBuilder.build({
            type: "insert",
            columns: {
               id: true,
               foo: 'foo',
               alias: 'bar'
            },
            table: 'tbl_test',
            values: {
               id: 123,
               foo: 'hello',
               alias: null
            }
         });
      
         expect( sql ).toBe('INSERT INTO tbl_test (id,foo,bar) VALUES ($1,$2,$3)');
         expect( values ).toEqual([ 123, 'hello', null ]);
      });

      it('should insert multiple', () => {
         const [ sql, values ] = queryBuilder.build({
            type: "insert",
            columns: {
               id: true,
               foo: 'foo',
               alias: 'bar'
            },
            table: 'tbl_test',
            values: [
               {
                  id: 123,
                  foo: 'hello',
                  alias: null
               },
               {
                 id: 456,
                 foo: 'test',
                 invalid: 'should ignore',
                 alias: DEFAULT_INSERT_VALUE
               }
            ]
         });
      
         expect( sql ).toBe('INSERT INTO tbl_test (id,foo,bar) VALUES ($1,$2,$3),($4,$5,DEFAULT)');
         expect( values ).toEqual([ 123, 'hello', null, 456, 'test' ]);
      });

   });


   describe('UPDATE', () => {

      it('should update', () => {
         const [ sql, values ] = queryBuilder.build({
            type: 'update',
            columns: {
               foo: 'foo',
               bar: true,
               ignored: false,
            },
            table: 'tbl_test',
            values: {
               foo: 123,
               invalid: 'will be ignored',
               bar: { type: 'expression', content: [ 'LOWER(', { type: 'param', value: 'hello' }, ')' ] },
               ignored: 'test'
            },
            where: {
               foo: { $gt: 200 }
            }
         });

         expect( sql ).toBe('UPDATE tbl_test SET foo = $1, bar = LOWER($2) WHERE foo > $3');
         expect( values ).toEqual([ 123, 'hello', 200 ]);
      });


      // TODO : implement the following test

      // it('should update using aliases', () => {
      //    const [ sql, values ] = queryBuilder.build({
      //       type: 'update',
      //       columns: {
      //          foo: 'col_foo'
      //       },
      //       table: 'tbl_test',
      //       document: {
      //          foo: 'b',
      //       },
      //       where: {
      //          foo: 'a'
      //       }
      //    });

      //    expect( sql ).toBe('UPDATE tbl_test SET col_foo = $1 WHERE col_foo = $2');
      //    expect( values ).toEqual([ 'b', 'a' ]);
      // });

   });


   describe('DELETE', () => {

      it('should delte', () => {
         const [ sql, values ] = queryBuilder.build({
            type: 'delete',
            table: 'tbl_test',
            where: {
               a: { $gt: 10 }
            },
            limit: 3
         });;

         expect( sql ).toBe('DELETE FROM tbl_test WHERE a > $1 LIMIT $2');
         expect( values ).toEqual([ 10, 3 ]);
      });

   });


});